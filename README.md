# WiseUI Integration 
ARRC-UVR Wise UI 통합 프로젝트



## Contents

- Integration/
  - [WiseUIAppUnity](https://gitlab.com/arrc-uvr/wiseui/-/tree/develop/Integration/WiseUIAppUnity) 
    - Tcp 프로토콜을 이용해서 HoloLens2의 실시간 센싱 값들을 서버에 전송하고 처리된 결과를 화면에 출력하는 유니티 example.
  - [WiseUIServer](https://gitlab.com/arrc-uvr/wiseui/-/tree/develop/Integration/WiseUIServer)
	  - 전송된 Hololens2의 stream data를 처리하고 처리된 결과를 유니티로 전달하는 python 기반 소스
  - [WiseUIMobileUnity](https://gitlab.com/arrc-uvr/wiseui/-/tree/develop/Integration/WiseUIMobileUnity)
	  - WiseUIAppUnity프로젝트와 상호작용 가능한 모바일 어플리케이션
	  
- Modules/
  - [TCPServerPython](https://github.com/IkbeomJeon/WiseUI/tree/master/Applications/TCPServerPython)
    - 홀로렌즈2에서 전송한 데이터를 수신하고 출력하는 Tcp 서버. 스크립트 실행 시 접속 대기 상태가 되며 클라이언트(홀로렌즈)가 접속하면 매 프레임마다 획득한 데이터를 출력함. 연결이 끊기면 다시 접속 대기 상태로 전환 됨.

## Setup
```
git clone https://gitlab.com/arrc-uvr/wiseui.git
```

- 원격 서버에서의 작업이 필요한 경우, 해당 서버에서 [WiseUIServer](https://gitlab.com/arrc-uvr/wiseui/-/tree/develop/Integration/WiseUIServer) 환경 구성 필요
	- [WISEUIServer/README.md](https://gitlab.com/arrc-uvr/wiseui/-/blob/develop/Integration/WiseUIServer/README.md) 참조


## Sample dataset

- Sample recorded dataset (including object / hand)
```
https://www.dropbox.com/scl/fo/hhrr2ufa58a4i6w9oypjf/h?dl=0&rlkey=bupqtm0mvt05pl3kupyuudydi
```
```
https://nas.uvrlab.org/fsdownload/uUNqIkdqC/HoloLens2%20Recording%20Dataset
```

## Compatibility

- Unity 2020.3.21f1 (LTS)*
- Visual Studio 2019

\* To use it in Unity 2020.1 - 2021.1,



Point cloud sample not supported in Unity 2021.2 or later since OpenXR becomes the only supported pipeline with different way of obtaining the Unity world coordiante frame. Other functions shouldn't be influenced.




## Build 

1. Open Integration/WiseUIAppUnity  in Unity.
2. In MRTK Project Configurator, select "Show Setting"
3. In Project Settings install XR Plugin Management if it is not.
4. In XR Plugin Management, switch to Universar Windows Platform setting, then tick "Windows Mixed Reality"
 
5. In the Project tab, open `Scenes/HoloLens2 PV Camera Test.unity`.
6. Select MixedRealityToolkit Gameobject in the Hierarchy. In the Inspector, change the mixed reality configuration profile to `New MixtedRealityToolkitConfigurationProfile`
7. Go to Build Settings, switch target platform to UWP.
8. Hopefully, there is no error in the console. Go to Build Settings, change Target Device to HoloLens, Architecture to ARM64. Build the Unity project in a new folder (e.g. 'Build' folder).
9. After building the visual studio solution from Unity, go to `Build/WiseUIAppUnity/Package.appxmanifest` and modify the `<package>...</package>` and `<Capabilities>...</Capabilities>`  like this.

```xml 
<Package 
  xmlns:mp="http://schemas.microsoft.com/appx/2014/phone/manifest" 
  xmlns:uap="http://schemas.microsoft.com/appx/manifest/uap/windows10" 
  xmlns:uap2="http://schemas.microsoft.com/appx/manifest/uap/windows10/2" 
  xmlns:uap3="http://schemas.microsoft.com/appx/manifest/uap/windows10/3" 
  xmlns:uap4="http://schemas.microsoft.com/appx/manifest/uap/windows10/4" 
  xmlns:iot="http://schemas.microsoft.com/appx/manifest/iot/windows10" 
  xmlns:mobile="http://schemas.microsoft.com/appx/manifest/mobile/windows10" 
  xmlns:rescap="http://schemas.microsoft.com/appx/manifest/foundation/windows10/restrictedcapabilities" 
  IgnorableNamespaces="uap uap2 uap3 uap4 mp mobile iot rescap" 
  xmlns="http://schemas.microsoft.com/appx/manifest/foundation/windows10"> 
```

```xml
  <Capabilities>
    <rescap:Capability Name="perceptionSensorsExperimental" />
    <Capability Name="internetClient" />
    <Capability Name="internetClientServer" />
    <Capability Name="privateNetworkClientServer" />
    <uap2:Capability Name="spatialPerception" />
    <DeviceCapability Name="backgroundSpatialPerception"/>
    <DeviceCapability Name="webcam" />
  </Capabilities>
```

`<DeviceCapability Name="backgroundSpatialPerception"/>` is only necessary if you use IMU sensor. 

6. Save the changes. Open `Build/WiseUIAppUnity.sln`. Change the build type to Release/Master-ARM64-Device(or Remote Machine). Build - Deploy.


## WISEUI 2024 APP(Hololens2/Mobile) Deployment

Hololens2
- Connect your hololens to PC(https://learn.microsoft.com/ko-kr/windows/mixed-reality/develop/advanced-concepts/using-the-windows-device-portal) before start.
1. Download 'WiseUIHololens2_2024.1.0.0_ARM64.zip' from the link(https://www.dropbox.com/scl/fi/hujev0ny6vq74lhekjlwr/WiseUIHololens2_2024.1.0.0_ARM64.zip?rlkey=yk3idtok4bgok8e7i7vxlv81n&st=gsn8kwpb&dl=0) and unzip the file.
2. From main page of HL2 device, go to 'Views > Apps' in left menu bar.
3. From Deploy apps section, press choose file button and select the 'WiseUIAppUnity_2024.1.0.0_ARM64.appx' from the unziped folder.
4. Run the app installed in Hololens2.

Mobile
- Connect your smartphone to PC
1. Download 'WiseUIMobile.apk' from the link(https://www.dropbox.com/scl/fi/0ktgae6y5beplwdlwaw63/WiseUIMobile.apk?rlkey=kwy24zte5ic10izzckxkp1usu&st=zm786ggs&dl=0).
2. Move the file to your smartphone.
3. Choose to install the apk file from your smartphone.
4. Run the app installed in smartphone.


## Acknowledgement
- This work was supported by Institute of Information & communications Technology Planning & Evaluation (IITP) grant funded by the Korea government(MSIT) (No.2019-0-01270, WISE AR UI/UX Platform Development for Smartglasses)

- [HoloLens2Stream](https://github.com/IkbeomJeon/HoloLens2Stream)에서 Research Mode의 sensing 데이터 획득은 [이 프로젝트](https://github.com/petergu684/HoloLens2-ResearchMode-Unity)를 참고하여 구현 됨.





## Lisense
WiseUI Applications are released under a MIT license. For a list of all code/library dependencies (and associated licenses), please see Dependencies.md.(To be updated)

For a closed-source version of WiseUI's modules for commercial purposes, please contact the authors : uvrlab@kaist.ac.kr, ikbeomjeon@kaist.ac.kr



