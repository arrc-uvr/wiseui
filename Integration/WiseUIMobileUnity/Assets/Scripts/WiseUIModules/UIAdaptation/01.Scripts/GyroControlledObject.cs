using UnityEngine;

public class GyroControlledObject : MonoBehaviour
{
    private Gyroscope gyro;
    private bool gyroEnabled;
    private Quaternion initialRotation;

    void Start()
    {
        // 자이로스코프 활성화
        gyroEnabled = EnableGyro();

        // 초기 회전을 기록
        if (gyroEnabled)
        {
            initialRotation = Quaternion.Euler(90f, 0f, 0f);
        }
    }

    void Update()
    {
        if (gyroEnabled)
        {
            // 자이로스코프의 회전 값 (attitude)을 가져와 초기 회전을 적용
            Quaternion deviceRotation = ConvertGyroRotation(Input.gyro.attitude);
            transform.rotation = initialRotation * deviceRotation;
        }
    }

    private bool EnableGyro()
    {
        // 자이로스코프 지원 여부 확인
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            return true;
        }
        return false;
    }

    private Quaternion ConvertGyroRotation(Quaternion gyroAttitude)
    {
        // 자이로스코프의 쿼터니언 값을 Unity 좌표계로 변환
        return new Quaternion(gyroAttitude.x, gyroAttitude.y, -gyroAttitude.z, -gyroAttitude.w);
    }
}
