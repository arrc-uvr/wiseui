﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneTouchSender : MonoBehaviour
{
    [SerializeField]
    private PhotonView PV;

    private void Awake()
    {
        PV = GameObject.FindGameObjectWithTag("RPCFunction").GetComponent<PhotonView>();
    }

    public void OnPointerDown()
    {
        //Debug.Log("OnPointerDown");
        PV.RPC("RPC_PointerDown", RpcTarget.All);
    }

    public void OnPointerUp()
    {
        //Debug.Log("OnPointerUp");
        PV.RPC("RPC_PointerUp", RpcTarget.All);
    }

    public void OnPhoneSwipeButtonClick()
    {
        Debug.Log("OnPhoneSwipeClick");
        PV.RPC("RPC_PhoneSwipeButtonClick", RpcTarget.All);
    }

    public void OnPhoneGyroButtonClick()
    {
        Debug.Log("OnPhoneGyroClick");
        PV.RPC("RPC_PhoneGyroButtonClick", RpcTarget.All);
    }

    public void OnGlassesGyroButtonClick()
    {
        Debug.Log("OnGlassesGyroClick");
        PV.RPC("RPC_GlassesGyroButtonClick", RpcTarget.All);
    }
}
