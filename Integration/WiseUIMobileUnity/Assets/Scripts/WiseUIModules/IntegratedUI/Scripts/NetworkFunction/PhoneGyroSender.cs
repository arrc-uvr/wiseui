﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneGyroSender : MonoBehaviour
{
    [SerializeField]
    private PhotonView PV;
    [SerializeField]
    private Transform phoneViz; 
    private Gyroscope gyro;
    private bool gyroEnabled;
    private Quaternion initialRotation;

    private bool isPhoneGyroSenderOn;

    private void Awake()
    {
        PV = GameObject.FindGameObjectWithTag("RPCFunction").GetComponent<PhotonView>();
    }

    void Start()
    {
        Input.gyro.enabled = true;
        Input.gyro.updateInterval = 0.0167f;
        initialRotation = Quaternion.Euler(90f, 0f, 0f);
        DeviceState.Instance.ViewMode = false;
    }

    void Update()
    {

        // 자이로스코프의 회전 값 (attitude)을 가져와 초기 회전을 적용
        Quaternion deviceRotation = ConvertGyroRotation(Input.gyro.attitude);
        DeviceState.Instance.GyroAttitude = initialRotation * deviceRotation;   //전송 불필요할거같아서 RPC말고 여기서 그냥 로컬 업데이트
        phoneViz.rotation = initialRotation * deviceRotation;
        // Gyro Attitude 전송기능 작동 확인했음. 당장은 안쓸거같아서 주석처리, 나중에 필요하면 풀고 사용
        /*
        PV.RPC("RPC_SyncGyroAttitude", RpcTarget.All, initialRotation * deviceRotation);
        */
        
        if (DeviceState.Instance.ObjControlMode == ObjControlMode.PhoneGyro && DeviceState.Instance.IsObjBeingManip)
        {
            PV.RPC("RPC_SyncGyroDelta", RpcTarget.All, RotDelYXtoScrDelXZ());
        }

        // 스마트폰 각도에 따른 트리거 기능 구현
        Vector3 eulerRotation = DeviceState.Instance.GyroAttitude.eulerAngles;
        float pitch = eulerRotation.x;
        // 에디터상 -180~180 표시되지만, 코드상에선 0~360을 쓰기때문에 에디터 방식으로 변환해줌
        if (pitch > 180f)
        {
            pitch -= 360f;
        }
        if (!DeviceState.Instance.ViewMode && pitch < 20 && pitch > -10)
        {
            PV.RPC("RPC_ViewModeChange", RpcTarget.All, true);
        }
        else if(DeviceState.Instance.ViewMode && pitch > 20 || pitch < -10)
        {
            PV.RPC("RPC_ViewModeChange", RpcTarget.All, false);
        }
    }

    private Quaternion ConvertGyroRotation(Quaternion gyroAttitude)
    {
        // 자이로스코프의 쿼터니언 값을 Unity 좌표계로 변환
        return new Quaternion(gyroAttitude.x, gyroAttitude.y, -gyroAttitude.z, -gyroAttitude.w);
    }

    // RPC를 통해 보내줄 부분
    public Vector3 RotDelYXtoScrDelXZ()
    {
        return new Vector3(Input.gyro.rotationRateUnbiased.y * Time.deltaTime, 0, -Input.gyro.rotationRateUnbiased.x * Time.deltaTime);
    }
}
