using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class SettingScript : MonoBehaviour, UIControllerInterface
{
    public string MyURL { get; set; }
    public TemplateContainer MyRoot { get; set; }

    private string gazeInteractionModeUxmlPath = "UXML/SettingItems/GazeInteractionMode";
    private Button gazeInteractionModeButton;
    public void Initialize(string url, TemplateContainer root)
    {
        MyURL = url;
        MyRoot = root;
        gazeInteractionModeButton = MyRoot.Q<Button>("GazeInteractionModeButton");
        gazeInteractionModeButton.RegisterCallback<ClickEvent>(OnGazeInteractionModeButtonClicked);
    }
    private void OnGazeInteractionModeButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            TemplateContainer root = gameObject.GetComponent<PageController>().LoadNewPage(gazeInteractionModeUxmlPath, PageController.UICategory.Setting);
            gameObject.GetComponent<TabbedMenu>().ReflectToSettingContent(root);
        }
    }
}
