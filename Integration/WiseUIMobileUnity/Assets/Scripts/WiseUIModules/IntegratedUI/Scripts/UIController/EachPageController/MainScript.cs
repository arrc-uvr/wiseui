using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MainScript : MonoBehaviour, UIControllerInterface
{
    public string MyURL { get; set; }
    public TemplateContainer MyRoot { get; set; }

    // 버튼에 매핑될 다음 페이지 경로
    private string listOfExhibitsUxmlPath = "UXML/MainItems/ListOfExhibits";
    private Button _listOfExhibitsButton;

    // TebbedMenu에서 메인 페이지에 갈아끼고 본 스크립트의 인스턴스를 생성 및 관리 해주므로 여기서는 내부 버튼 기능만 매핑해주면 됨
    public void Initialize(string url, TemplateContainer root)
    {
        MyURL = url;
        MyRoot = root;
        _listOfExhibitsButton = MyRoot.Q<Button>("ListOfExhibitsButton");
        _listOfExhibitsButton.RegisterCallback<ClickEvent>(OnListOfExhibitsButtonButtonClicked);
    }

    private void OnListOfExhibitsButtonButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            Debug.Log("OnListOfExhibitsButtonButtonClicked");
            TemplateContainer root = gameObject.GetComponent<PageController>().LoadNewPage(listOfExhibitsUxmlPath, PageController.UICategory.UI);
            gameObject.GetComponent<TabbedMenu>().ReflectToMainContent(root);
        }
    }
}
