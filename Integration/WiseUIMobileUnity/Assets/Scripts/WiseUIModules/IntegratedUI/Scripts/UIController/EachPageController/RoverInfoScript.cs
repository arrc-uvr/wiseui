using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class RoverInfoScript : MonoBehaviour, UIControllerInterface
{
    [SerializeField]
    private PhotonView PV;
    public string MyURL { get; set; }
    public TemplateContainer MyRoot { get; set; }

    // 버튼에 매핑될 다음 페이지 경로
    private string rover_antennaInfoUxmlPath = "UXML/MainItems/ExhibitDetail/RoverDetail/Rover_antennaInfo";
    private string rover_headlampInfoUxmlPath = "UXML/MainItems/ExhibitDetail/RoverDetail/Rover_headlampInfo";
    private string rover_solarpanelnfoUxmlPath = "UXML/MainItems/ExhibitDetail/RoverDetail/Rover_solarpanelInfo";
    private Button _rover_antennaInfoUButton;
    private Button _rover_headlampInfoUButton;
    private Button _rover_solarpanelnfoUButton;
    private Button _rover_antennaGenUButton;
    private Button _rover_headlampGenUButton;
    private Button _rover_solarpaneGenUButton;

    private void Awake()
    {
        PV = GameObject.FindGameObjectWithTag("RPCFunction").GetComponent<PhotonView>();
    }

    // TebbedMenu에서 메인 페이지에 갈아끼고 본 스크립트의 인스턴스를 생성 및 관리 해주므로 여기서는 내부 버튼 기능만 매핑해주면 됨
    public void Initialize(string url, TemplateContainer root)
    {
        MyURL = url;
        MyRoot = root;
        _rover_antennaInfoUButton = MyRoot.Q<Button>("AntennaInfoButton");
        _rover_antennaInfoUButton.RegisterCallback<ClickEvent>(OnAntennaInfoButtonClicked);
        _rover_headlampInfoUButton = MyRoot.Q<Button>("HeadlampInfoButton");
        _rover_headlampInfoUButton.RegisterCallback<ClickEvent>(OnHeadlampInfoButtonButtonClicked);
        _rover_solarpanelnfoUButton = MyRoot.Q<Button>("SolarpanelInfoButton");
        _rover_solarpanelnfoUButton.RegisterCallback<ClickEvent>(OnSolarpanelInfoButtonClicked);

        _rover_antennaGenUButton = MyRoot.Q<Button>("AntennaGenButton");
        _rover_antennaGenUButton.RegisterCallback<ClickEvent>(OnAntennaGenButtonClicked);
        _rover_headlampGenUButton = MyRoot.Q<Button>("HeadlampGenButton");
        _rover_headlampGenUButton.RegisterCallback<ClickEvent>(OnHeadlampGenButtonClicked);
        _rover_solarpaneGenUButton = MyRoot.Q<Button>("SolarpanelGenButton");
        _rover_solarpaneGenUButton.RegisterCallback<ClickEvent>(OnSolarpanelGenButtonClicked);
    }

    private void OnAntennaInfoButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            //Debug.Log("OnAntennaInfoButtonClicked");
            TemplateContainer root = gameObject.GetComponent<PageController>().LoadNewPage(rover_antennaInfoUxmlPath, PageController.UICategory.UI);
            gameObject.GetComponent<TabbedMenu>().ReflectToMainContent(root);
        }
    }

    private void OnHeadlampInfoButtonButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            //Debug.Log("OnHeadlampInfoButtonButtonClicked");
            TemplateContainer root = gameObject.GetComponent<PageController>().LoadNewPage(rover_headlampInfoUxmlPath, PageController.UICategory.UI);
            gameObject.GetComponent<TabbedMenu>().ReflectToMainContent(root);
        }
    }

    private void OnSolarpanelInfoButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            //Debug.Log("OnSolarpanelInfoButtonClicked");
            TemplateContainer root = gameObject.GetComponent<PageController>().LoadNewPage(rover_solarpanelnfoUxmlPath, PageController.UICategory.UI);
            gameObject.GetComponent<TabbedMenu>().ReflectToMainContent(root);
        }
    }

    private void OnAntennaGenButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            PV.RPC("RPC_GenButtonClick", RpcTarget.All, "Antenna");
            Debug.Log("OnAntennaGenButtonClicked");
        }
    }

    private void OnHeadlampGenButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            PV.RPC("RPC_GenButtonClick", RpcTarget.All, "Headlamp");
            Debug.Log("OnHeadlampGenButtonClicked");
        }
    }

    private void OnSolarpanelGenButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            PV.RPC("RPC_GenButtonClick", RpcTarget.All, "Solarpanel");
            Debug.Log("OnSolarpanelGenButtonClicked");
        }
    }
}
