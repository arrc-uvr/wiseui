using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class GazeInteractionModeScript : MonoBehaviour, UIControllerInterface
{
    public string MyURL { get; set; }
    public TemplateContainer MyRoot { get; set; }

    private Button phoneSwipeModeButton;
    private Button phoneGyroModeButton;
    private Button glassesGyroModeButton;
    private PhoneTouchSender PhoneTouchSender;

    public void Initialize(string url, TemplateContainer root)
    {
        MyURL = url;
        MyRoot = root;
        PhoneTouchSender = GameObject.FindGameObjectWithTag("PhoneStateSender").GetComponent<PhoneTouchSender>();
        phoneSwipeModeButton = MyRoot.Q<Button>("PhoneSwipeModeButton");
        phoneSwipeModeButton.RegisterCallback<ClickEvent>(OnPhoneSwipeModeButtonClicked);
        phoneGyroModeButton = MyRoot.Q<Button>("PhoneGyroModeButton");
        phoneGyroModeButton.RegisterCallback<ClickEvent>(OnPhoneGyroModeButtonClicked);
        glassesGyroModeButton = MyRoot.Q<Button>("GlassesGyroModeButton");
        glassesGyroModeButton.RegisterCallback<ClickEvent>(OnGlassesGyroModeButtonClicked);  
    }
    private void OnPhoneSwipeModeButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            Debug.Log("OnPhoneSwipeModeButtonClicked");
            PhoneTouchSender.OnPhoneSwipeButtonClick();
        }
    }
    private void OnPhoneGyroModeButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            Debug.Log("OnPhoneGyroModeButtonClicked");
            PhoneTouchSender.OnPhoneGyroButtonClick();
        }
    }
    private void OnGlassesGyroModeButtonClicked(ClickEvent evt)
    {
        if (DeviceState.Instance.GazeOnObjFromGlasses == GazeOnObjFromGlasses.Null)
        {
            Debug.Log("OnGlassesGyroModeButtonClicked");
            PhoneTouchSender.OnGlassesGyroButtonClick();
        }
    }
}
