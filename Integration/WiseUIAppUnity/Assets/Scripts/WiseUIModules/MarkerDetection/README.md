# Marker Detector
ArUco Marker를 생성하고, 인식하여 pose estimation 하는 모듈
[Unity Store](https://assetstore.unity.com/packages/tools/integration/opencv-for-unity-21088) 패키지 import 필요 (ARRC unity 계정)
[HoloLens에서 세팅 참고](https://github.com/EnoxSoftware/HoloLensWithOpenCVForUnityExample)

- 'TagGenerator' prefab을 사용하여 ArUco Tag와 calibaration에 필요한 GridBoard, CharUcoBoard 생성
- (추가예정)




## Lisense
WiseUI Applications are released under a MIT license. For a list of all code/library dependencies (and associated licenses), please see Dependencies.md.(To be updated)

For a closed-source version of WiseUI's modules for commercial purposes, please contact the authors : uvrlab@kaist.ac.kr, ikbeomjeon@kaist.ac.kr



