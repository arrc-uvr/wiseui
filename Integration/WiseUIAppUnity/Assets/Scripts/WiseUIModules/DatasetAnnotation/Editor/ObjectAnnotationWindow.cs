using ARRC.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using WiseUI.Simulator;
using static Codice.Client.Common.Servers.RecentlyUsedServers;

public class AnnoConfig
{

}
public class ObjectAnnotationWindow : EditorWindow
{
    string conf_filepath;
    Vector3 scrollPosition;

    List<GameObject> bboxes = new List<GameObject>();
    string dataset_path;

    private void OnGUI()
    {
        if (GUILayout.Button("Select Recording Path"))
            conf_filepath = EditorUtility.OpenFilePanel("Select configuratio file", conf_filepath, "");

        GUIStyle headerStyle = new GUIStyle(EditorStyles.label);
        headerStyle.alignment = TextAnchor.MiddleCenter;

        EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
        GUILayout.Label("id", headerStyle, GUILayout.Width(40));
        GUILayout.Label("name", headerStyle);
        GUILayout.Box("", GUIStyle.none, GUILayout.Width(50));
        EditorGUILayout.EndHorizontal();

        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
        GUIContent useContent = new GUIContent(">", "Restore");
        GUIContent deleteContent = new GUIContent("X", "Remove");

        int removeIdx = -1;
        int openIdx = -1;
        for (int i = 0; i < bboxes.Count; i++)
        {
            GameObject item = bboxes[i];
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label((i + 1).ToString(), GUILayout.Width(40));
            //GUILayout.Label(((DTConfigurationItem)item).title);
            item.gameObject.name = GUILayout.TextField(item.gameObject.name, GUILayout.Width(120));

            //DateTime time = new DateTime(item.timestamp);

            //GUILayout.Label(time.ToString("yyyy-MM-dd HH:mm"), GUILayout.Width(120));

            if (GUILayout.Button(useContent, GUILayout.Width(20))) openIdx = i;

            if (GUILayout.Button(deleteContent, GUILayout.Width(20))) removeIdx = i;

            EditorGUILayout.EndHorizontal();
        }

        if (removeIdx >= 0)
        {
            DestroyImmediate(bboxes[removeIdx]);
            bboxes.RemoveAt(removeIdx);
        }


        if (GUILayout.Button("Add annotation"))
        {
            bboxes.Add(new GameObject());
        }
        EditorGUILayout.EndScrollView();
        if (GUILayout.Button("Generate dataset"))
        {

        }


    }
    [MenuItem("WiseUI/Annotation Window")]
    public static ObjectAnnotationWindow OpenWindow()
    {
        return Instance;
    }

    static ObjectAnnotationWindow instance;

    public static ObjectAnnotationWindow Instance
    {
        get
        {
            if (instance)
                return instance;
            else
            {
                instance = GetWindow<ObjectAnnotationWindow>(false, "Annotation Window");

                return instance;
            }
        }
    }
}
