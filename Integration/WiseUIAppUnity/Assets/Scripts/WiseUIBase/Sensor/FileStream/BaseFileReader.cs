using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace WiseUI.Base
{
    [Serializable]
    public class BaseFileReader : BaseSensorReader
    {
        [SerializeField]
        protected long timestamp_start, timestamp_end;

        [SerializeField]
        protected string dataset_path;

        [SerializeField]
        protected SerializableDictionary<long, HoloLensSensorFrame> sensorFrames = new SerializableDictionary<long, HoloLensSensorFrame>();

        public long Timestamp_Start
        {
            get { return timestamp_start; }
        }
        public long Timestamp_End
        {
            get { return timestamp_end; }
        }

        public int TotalFrameCount
        {
            get { return sensorFrames.Count; }
        }
        
        public bool IsNewFrame(long target_timestamp)
        {
            if (target_timestamp < timestamp_start)
                return false;
            
            var closest_timestamp = GetCloestTimestamp(target_timestamp);
            
            if (timestamp != closest_timestamp)
                return true;
            
            else
                return false;
       
        }

        public void UpdateLatestTexture(long target_timestamp)
        {
            var closest_timestamp = GetCloestTimestamp(target_timestamp);
            
            timestamp = closest_timestamp;
        }

        long GetCloestTimestamp(long target_timestamp)
        {
            if (target_timestamp < timestamp_start)
            {
                throw new Exception("target_timestamp is out of range");
            }
            var registed_timestamps = sensorFrames.Keys.ToList();
            
            //find cloest timestamp,  where the timestamp is smaller than current timestamp.
            long cloestTimestamp = timestamp_start;
            for (int i = 0; i < registed_timestamps.Count; i++)
            {
                if (registed_timestamps[i] <= target_timestamp && registed_timestamps[i] >= cloestTimestamp)
                    cloestTimestamp = registed_timestamps[i];
                else
                    break;
            }

            return cloestTimestamp;
        }
    }

}
