using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Client
{
    public delegate void ReceiveCallback(byte[] buffer);
    
    public abstract void Connect(string serverIP, int serverPort);
    public abstract void Disconnect();
    public abstract void Send(ref byte[] buffer);
    public abstract void BeginReceive(ReceiveCallback receiveCallback);
    public abstract bool IsConnected();
    

}
