using Microsoft.MixedReality.Toolkit.Experimental.UI;
using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using TMPro;
using UnityEngine;
using System;
using WiseUI.Base;
using WiseUI.Modules;
using static TMPro.SpriteAssetUtilities.TexturePacker_JsonArray;
using System.Threading;
using UnityEngine.Assertions;

public class FlowManager : MonoBehaviour
{
    // Modules
    //public HoloLens2PVCameraReader pvCameraReader;
    //public SocketClientManager socketClient;
    public ARRCObjectronDetector objectDetector;

    public Interactable confButton;

    // Title UI
    public TextMeshPro stateMessage;
    public Interactable closeButton;


    // TCP UI
    public Interactable connectButton;
    public MRTKTMPInputField hostIPField, portField;

    public GameObject images;

    //Capture UI
    public Interactable startCaptureButton;
    public InteractableToggleCollection pvCamToggles;
    public GameObject pvImagePlane;
    Coroutine cameraTextureUpdateHandle;

    //Detection UI
    public Interactable startDetectionButton;
    public InteractableToggleCollection detectionToggles;
    public GameObject detectedImagePlane;
    Coroutine detectionUpdateHandle;
    //Camera Image planes

    Coroutine sendImageDataHandle;

    private void Awake()
    {
        //pvCameraReader = GameObject.Find("WiseUI Agent").GetComponent<HoloLens2PVCameraReader>();
        //socketClient = GameObject.Find("WiseUI Agent").GetComponent<SocketClientManager>();
        //objectDetector = GameObject.Find("WiseUI Agent").GetComponent<ARRCObjectronDetector>();

        confButton = transform.Find("Setting").GetComponent<Interactable>();
        confButton.OnClick.AddListener(OnConfigurationButtonClick);
        images = transform.Find("Images").gameObject;
        pvImagePlane = transform.Find("Images/PVImagePlane").gameObject;
        detectedImagePlane = transform.Find("Images/DetectedImagePlane").gameObject;
        //transform.Find("Pannel").gameObject.SetActive(false);

        //Title state
        stateMessage = transform.Find("Pannel/TitleBar/Title").GetComponent<TextMeshPro>();
        closeButton = transform.Find("Pannel/TitleBar/TitleButton/Close").GetComponent<Interactable>();
        closeButton.OnClick.AddListener(CloseButtonClick);

        //TCP Connect
        hostIPField = transform.Find("Pannel/HostAddress/Host IP").GetComponent<MRTKTMPInputField>();
        portField = transform.Find("Pannel/HostAddress/Port").GetComponent<MRTKTMPInputField>();
        connectButton = transform.Find("Pannel/Connect").GetComponent<Interactable>();
        connectButton.OnClick.AddListener(ConnectButtonClick);

        //PV sensor resolution.
        pvCamToggles = transform.Find("Pannel/PVSensorGroup").GetComponent<InteractableToggleCollection>();
        startCaptureButton = transform.Find("Pannel/StartCapture").GetComponent<Interactable>();
        startCaptureButton.OnClick.AddListener(OnStartCaptureButtonClick);

        //Detection target.
        detectionToggles = transform.Find("Pannel/ModelGroup").GetComponent<InteractableToggleCollection>();
        startDetectionButton = transform.Find("Pannel/StartDetection").GetComponent<Interactable>();
        startDetectionButton.OnClick.AddListener(OnStartDetectionButtonClick);


    }
    private void Start()
    {
        LoadUIContents();
        //images.SetActive(false);
        pvImagePlane.SetActive(false);
        detectedImagePlane.SetActive(false);
    }
    void LoadUIContents()
    {
        ConfigurationManager.Instance.Load();
        hostIPField.text = ConfigurationManager.Instance.hostIP;
        portField.text = ConfigurationManager.Instance.port.ToString();
        pvCamToggles.SetSelection((int)ConfigurationManager.Instance.pvCameraType);
    }
    void SaveUIContents()
    {
        string ip = hostIPField.text;
        int port = int.Parse(portField.text);
        PVCameraType pVCameraType = (PVCameraType)pvCamToggles.CurrentIndex;
        ConfigurationManager.Instance.Save(ip, port, pVCameraType);
        Debug.Log(string.Format("{0}, {1},{2}", ip, port, pVCameraType.ToString()));
    }
    void OnConfigurationButtonClick()
    {
        LoadUIContents();
    }

    void CloseButtonClick()
    {
        SaveUIContents();
    }
    private void OnDestroy()
    {
        SaveUIContents();
    }
    void ConnectButtonClick()
    {
        //Debug.Log(hostIPField.text);

        if (!connectButton.IsToggled)
        {
            SocketClientManager.Instance.Disconnect();
            stateMessage.text = string.Format("Success to disconnect");
            return;
        }

        try
        {
            string ip = hostIPField.text;
            int port = int.Parse(portField.text);

            SocketClientManager.Instance.Connect(ip, port);
            stateMessage.color = Color.white;
            stateMessage.text = string.Format("Success to connect : {0}:{1}", ip, port);
            StartCoroutine(Process_received_data());
        }
        catch(Exception e)
        {
            stateMessage.text = string.Format("Fail to connect : {0}", e.Message);
            stateMessage.color = Color.red;
            connectButton.IsToggled = false;
            StopCoroutine(Process_received_data());
        }
    }

    void OnStartCaptureButtonClick()
    {
        try
        {
            if (!startCaptureButton.IsToggled)
            {
                HoloLens2SensorStreamManager.Instance.PVCamera.StopPVCamera();
                pvImagePlane.SetActive(false);

                if (cameraTextureUpdateHandle != null)
                    StopCoroutine(cameraTextureUpdateHandle);

                return;
            }

            int idx = pvCamToggles.CurrentIndex;
            pvImagePlane.SetActive(true);
            HoloLens2SensorStreamManager.Instance.PVCamera.StartPVCamera((PVCameraType)idx);
            cameraTextureUpdateHandle = StartCoroutine(UpdateCameraTexutre());

        }
        catch(System.Exception e)
        {
            stateMessage.text = string.Format("Fail : {0}", e.Message);
            stateMessage.color = Color.red;
            startCaptureButton.IsToggled = false;
        }

    }

    void OnStartDetectionButtonClick()
    {
        try
        {
            if (!startDetectionButton.IsToggled)
            {
                detectedImagePlane.SetActive(false);

                if (detectionUpdateHandle != null)
                    StopCoroutine(detectionUpdateHandle);
                return;
            }

            detectedImagePlane.SetActive(true);
            int idx = detectionToggles.CurrentIndex;
            detectionUpdateHandle = StartCoroutine(UpdateDetection());


            //objectDetector.LoadModel((ModelType)idx);
            //stateMessage.text = string.Format("Load Model OK.");
        }
        catch (System.Exception e)
        {
            stateMessage.text = string.Format("Fail : {0}", e.Message);
            stateMessage.color = Color.red;
            startDetectionButton.IsToggled = false;
        }
    }
    IEnumerator UpdateCameraTexutre()
    {
        while (true)
        {
            if (HoloLens2SensorStreamManager.Instance.PVCamera.IsNewFrame)
            {
                HoloLens2SensorStreamManager.Instance.PVCamera.UpdateLatestTexture();
                Texture2D latestTexture = HoloLens2SensorStreamManager.Instance.PVCamera.GetLastestTexture();
                pvImagePlane.GetComponent<MeshRenderer>().material.mainTexture = latestTexture;

                if(connectButton.IsToggled)
                {
                    var dataPackage = new RGBImageDataPackage(HoloLens2SensorStreamManager.Instance.PVCamera.Timestamp, latestTexture);
                    var data = dataPackage.Encode();
                    SocketClientManager.Instance.Send(ref data);
                }
                    

                //if (startDetectionButton.IsToggled)
                //{
                //    objectDetector.Run(latestTexture);
                //}

                //socketClient.SendRGBImage(pvCameraReader.FrameID, latestTexture, ImageCompression.None);
                //float time_to_send = Time.time - start_time;
                //DebugText.Instance.lines["Time_to_send"] = time_to_send.ToString();
            }

            yield return new WaitForEndOfFrame();
        }
    }
    
    IEnumerator Process_received_data()
    {
        while(true)
        {
            if(SocketClientManager.Instance.IsNewHandDataReceived)
            {
                var frameData = SocketClientManager.Instance.GetLatestReceivedData(); // 만약 새로운 frameData가 도착하지 않았다면 NoDataReceivedExecption이 이 함수 내부에서 발생한다.
                DebugText.Instance.lines["frame_id"] = frameData.frameInfo.frameID.ToString();
                DebugText.Instance.lines["delay_comm"] = frameData.frameInfo.GetTotalDelay().ToString();
                SocketClientManager.Instance.IsNewHandDataReceived = false;
            }
       
            yield return null;
        }
  
    }
    IEnumerator UpdateDetection()
    {
        while (true)
        {

            yield return new WaitForEndOfFrame();
        }
    }
}

